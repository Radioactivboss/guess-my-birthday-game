from random import randint
name = input("Hi! What's your name?")
for guess in range(1,6):
    month = randint(1,12)
    year = randint(1924,2004)
    print("Guess", name, "were you born in",
        month, "/", year, "?")
    response = input("yes or no?")
    if response == "yes":
        print("I knew it!")
        exit()
    elif guess == 5:
        print("I have other things to do. Good bye.")
        exit()
    else:
        print("Drat! Lemme try again!")